///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
//  @brief  Lab 07c - hello1 - EE 205 - Spr 2022
//
// @file hello1.c
//
// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
// @date   02/24/2022
//
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main() {
   cout << "Hello World!" << endl;

}
