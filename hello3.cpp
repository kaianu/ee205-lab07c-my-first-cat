///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
//  @brief  Lab 07c - hello3 - EE 205 - Spr 2022
//
// @file hello3.c
//
// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
// @date   02/24/2022
//
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

class Cat {
public:
   void sayHello() {
      cout << "Meow" << endl;
   }
} ;

int main() {
   Cat myCat;
   myCat.sayHello();
}
