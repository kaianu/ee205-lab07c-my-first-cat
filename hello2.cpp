///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
//  @brief  Lab 07c - hello2 - EE 205 - Spr 2022
//
// @file hello2.c
//
// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
// @date   02/24/2022
//
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() {
   std::cout << "Hello World!" << std::endl;

}
